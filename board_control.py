# -*- coding: utf-8 -*-
#
# サッポロ電子クラフト部 シーズン５ 動物将棋 ボード制御プログラム
#
#  
# ポート割り当て
#  IO エキスパンダー 
#  MCP23017 を I2C 接続、アドレスは 20h,21h,22h
#  メインの盤の LED 全部とスイッチの一部を制御
#  LED は High で点灯, SW は ON で LOW
#  20h - U1
#    ポート    機能
#    GPA0-3 - GC1-4
#    GPA4-7 - A1-4
#    GPB0-3 - GA1-4
#    GPB4-7 - GB1-4
#
#  21h - U2
#    ポート    機能
#    GPA0-3 - RC1-4
#    GPA4-7 - B1-4
#    GPB0-3 - RA1-4
#    GPB4-7 - RB1-4
#
#  機能 
#    R(座標)  メインの盤の赤色 LED
#    G(座標)  メインの盤の緑色 LED
#    (座標)   メインの盤の座標の位置のスイッチ
#  例
#    RA1 座標A1の赤色 LED
#    GC4 座標C4の緑色 LED
#    B3  座標B3のスイッチ
#
#  盤の座標
#  メインの盤
#   +--+--+--+
#   |A1|B1|C1|
#   +--+--+--+
#   |A2|B2|C2|
#   +--+--+--+
#   |A3|B3|C3|
#   +--+--+--+
#   |A4|B4|C4|
#   +--+--+--+
#
# Raspberry Pi
#  メインの盤のスイッチの一部と駒台のスイッチ全部
#   駒台のスイッチは、位置は関係ない
#   先手の駒台(先手=Play First)
#    SP1 - GPIO-11
#    SP2 - GPIO-12
#    SP3 - GPIO-13
#    SP4 - GPIO-14
#    SP5 - GPIO-15
#    SP6 - GPIO-16
#   後手の駒台(先手=Draw First)
#    SD1 - GPIO-21
#    SD2 - GPIO-22
#    SD3 - GPIO-23
#    SD4 - GPIO-24
#    SD5 - GPIO-25
#    SD6 - GPIO-26
#   メインの盤の一部
#     C1 - GPIO-5
#     C2 - GPIO-6
#     C3 - GPIO-7
#     C4 - GPIO-8
#
import smbus
import time

CHANNEL   = 1      # i2c割り当てチャンネル 1 or 0
IC_U1_ADDR    = 0x20   # IC U1 アドレス
IC_U2_ADDR    = 0x21   # IC U2 アドレス
IC_U3_ADDR    = 0x22   # IC U3 アドレス
REG_IODIRA = 0x00   # 入出力設定レジスタA
REG_IODIRB = 0x01   # 入出力設定レジスタB
REG_GPPUA  = 0x0C	# pull-up 設定A
REG_GPPUB  = 0x0D	# pull-up 設定B
REG_GPIOA  = 0x12   # 入力ポートの値 A
REG_GPIOB  = 0x13   # 入力ポートの値 B
REG_OLATA  = 0x14   # 出力レジスタA
REG_OLATB  = 0x15   # 出力レジスタB

class	DobutuBoard(object):
	MAIN_BOARD_X_COUNT = 3		# 横の数 (A, B, C)
	MAIN_BOARD_Y_COUNT = 4		# 縦の数 (1, 2, 3, 4)
	mainBoardSwStatus = [[0 for x in range(MAIN_BOARD_X_COUNT)] for y in range(MAIN_BOARD_Y_COUNT)]

	SUB_BOARD_SW_NUM = 6	# 駒台のスイッチの数
	subBoardPlayFirstSwStatus = [0 for x in range(SUB_BOARD_SW_NUM)]
	subBoardDrawFirstSwStatus = [0 for x in range(SUB_BOARD_SW_NUM)]
	RED = 0
	GREEN = 1
	PLAY_FIRST = 0	# 先手
	DRAW_FIRST = 1	# 後手

	bus = smbus.SMBus(CHANNEL)                          #        D76543210

	# LED にアクセスするための情報 (I2C アドレス, レジスタ, bitmask)
	# Green LED のマッピング
	tableMapGreen = (
		((IC_U1_ADDR, REG_OLATB, 0x01), (IC_U2_ADDR, REG_OLATB, 0x01), (IC_U3_ADDR, REG_OLATB, 0x01)),
		((IC_U1_ADDR, REG_OLATB, 0x08), (IC_U2_ADDR, REG_OLATB, 0x08), (IC_U3_ADDR, REG_OLATB, 0x08)),
		((IC_U1_ADDR, REG_OLATA, 0x80), (IC_U2_ADDR, REG_OLATA, 0x80), (IC_U3_ADDR, REG_OLATA, 0x80)),
		((IC_U1_ADDR, REG_OLATA, 0x10), (IC_U2_ADDR, REG_OLATA, 0x10), (IC_U3_ADDR, REG_OLATA, 0x10)),
	)
	# Red LED のマッピング
	tableMapRed = (
		((IC_U1_ADDR, REG_OLATB, 0x02), (IC_U2_ADDR, REG_OLATB, 0x02), (IC_U3_ADDR, REG_OLATB, 0x02)),
		((IC_U1_ADDR, REG_OLATB, 0x10), (IC_U2_ADDR, REG_OLATB, 0x10), (IC_U3_ADDR, REG_OLATB, 0x10)),
		((IC_U1_ADDR, REG_OLATA, 0x40), (IC_U2_ADDR, REG_OLATA, 0x40), (IC_U3_ADDR, REG_OLATA, 0x40)),
		((IC_U1_ADDR, REG_OLATA, 0x08), (IC_U2_ADDR, REG_OLATA, 0x08), (IC_U3_ADDR, REG_OLATA, 0x08)),
	)
	# LED 全体のマッピング
	tableMap = (tableMapRed, tableMapGreen)

	# メインボードのスイッチのマッピング
	tableSwMapMain = (	# MainBoard のスイッチにアクセスするための情報 (Ux, PORT, bitmask)
		((IC_U1_ADDR, REG_GPIOB, 0x04), (IC_U2_ADDR, REG_GPIOB, 0x04), (IC_U3_ADDR, REG_GPIOB, 0x04)),
		((IC_U1_ADDR, REG_GPIOB, 0x20), (IC_U2_ADDR, REG_GPIOB, 0x20), (IC_U3_ADDR, REG_GPIOB, 0x20)),
		((IC_U1_ADDR, REG_GPIOA, 0x20), (IC_U2_ADDR, REG_GPIOA, 0x20), (IC_U3_ADDR, REG_GPIOA, 0x20)),
		((IC_U1_ADDR, REG_GPIOA, 0x04), (IC_U2_ADDR, REG_GPIOA, 0x04), (IC_U3_ADDR, REG_GPIOA, 0x04)),
	)
	
	# 先手駒台のスイッチのマップ
	tableSwMapSubPlayFirst = (
		(IC_U2_ADDR, REG_GPIOB, 0x40),
		(IC_U2_ADDR, REG_GPIOB, 0x80),
		(IC_U1_ADDR, REG_GPIOB, 0x40),
		(IC_U1_ADDR, REG_GPIOB, 0x80),
		(IC_U1_ADDR, REG_GPIOA, 0x02),
		(IC_U1_ADDR, REG_GPIOA, 0x01),
	)
	
	tableSwMapSubDrawFirst = (
		(IC_U2_ADDR, REG_GPIOA, 0x02),
		(IC_U2_ADDR, REG_GPIOA, 0x01),
		(IC_U3_ADDR, REG_GPIOB, 0x40),
		(IC_U3_ADDR, REG_GPIOB, 0x80),
		(IC_U3_ADDR, REG_GPIOA, 0x02),
		(IC_U3_ADDR, REG_GPIOA, 0x01),
	)

	def __init__(self):
		# エキスパンダーの初期設定
		self.writeByteData(IC_U1_ADDR, REG_IODIRA, 0x27)	#U1 GPA   OOIOOIII
		self.writeByteData(IC_U1_ADDR, REG_GPPUA , 0x27)	#U1 GPA 入力ポートは pull-up オン
		self.writeByteData(IC_U1_ADDR, REG_IODIRB, 0xe4)	#U1 GPB   IIIOOIOO
		self.writeByteData(IC_U1_ADDR, REG_GPPUB , 0xe4)	#U1 GPB 入力ポートは pull-up オン
		self.writeByteData(IC_U1_ADDR, REG_OLATA , 0x00)	#U1 GPA 出力ポートは 0
		self.writeByteData(IC_U1_ADDR, REG_OLATB , 0x00)	#U1 GPB 出力ポートは 0

		self.writeByteData(IC_U2_ADDR, REG_IODIRA, 0x27)	#U2 GPA   OOIOOIII
		self.writeByteData(IC_U2_ADDR, REG_GPPUA , 0x27)	#U2 GPA 入力ポートは pull-up オン
		self.writeByteData(IC_U2_ADDR, REG_IODIRB, 0xe4)	#U2 GPB   IIIOOIOO
		self.writeByteData(IC_U2_ADDR, REG_GPPUB , 0xe4)	#U2 GPB 入力ポートは pull-up オン
		self.writeByteData(IC_U2_ADDR, REG_OLATA , 0x00)	#U1 GPA 出力ポートは 0
		self.writeByteData(IC_U2_ADDR, REG_OLATB , 0x00)	#U1 GPB 出力ポートは 0

		self.writeByteData(IC_U3_ADDR, REG_IODIRA, 0x27)	#U3 GPA   OOIOOIII
		self.writeByteData(IC_U3_ADDR, REG_GPPUA , 0x27)	#U3 GPA 入力ポートは pull-up オン
		self.writeByteData(IC_U3_ADDR, REG_IODIRB, 0xe4)	#U3 GPB   IIIOOIOO
		self.writeByteData(IC_U3_ADDR, REG_GPPUB , 0xe4)	#U3 GPB 入力ポートは pull-up オン
		self.writeByteData(IC_U3_ADDR, REG_OLATA , 0x00)	#U3 GPA 出力ポートは 0
		self.writeByteData(IC_U3_ADDR, REG_OLATB , 0x00)	#U3 GPB 出力ポートは 0

	def writeByteData(self, adr, reg, value):
		if adr != IC_U3_ADDR:
			self.bus.write_byte_data(adr, reg, value)
			
	def readByteData(self, adr, reg):
		if adr != IC_U3_ADDR:
			return self.bus.read_byte_data(adr, reg)
		else:
			return 0xff

	def setLedState(self, x, y, color, state):
		paras = self.tableMap[color][y][x]
		tmp = self.readByteData(paras[0], paras[1])
		if state:
			tmp |= paras[2]
		else:
			tmp &= ~paras[2]
		self.writeByteData(paras[0], paras[1], tmp)

	#
	# エキスパンダーの指定されたビットの状態を返す
	#   adr   I2C adrress   (U1=0x20  U2=0x21)
	#   port  A or B  (A=REG_GPIOA B=REG_GPIOB)
	#   bitmask   bit を mask で指定
	#
	def getExpnderSwStatus(self, paras):
		return False if (self.readByteData(paras[0], paras[1]) & paras[2]) else True 

	def getMainBoardSwStatus(self):
		for y in range(self.MAIN_BOARD_Y_COUNT):
			for x in range(self.MAIN_BOARD_X_COUNT):
				self.mainBoardSwStatus[y][x] = self.getExpnderSwStatus(self.tableSwMapMain[y][x])
		
	def getSubBoardSwStatus(self):
		for i in range(self.SUB_BOARD_SW_NUM):
			self.subBoardPlayFirstSwStatus[i] = self.getExpnderSwStatus(self.tableSwMapSubPlayFirst[i])
			self.subBoardDrawFirstSwStatus[i] = self.getExpnderSwStatus(self.tableSwMapSubDrawFirst[i])
		return self.subBoardPlayFirstSwStatus, self.subBoardDrawFirstSwStatus

	def getSwState(self):
		return self.getMainBoardSwStatus(), self.getSubBoardSwStatus()

'''
ICADDR    = 0x20   # スレーブ側ICアドレス
bus = smbus.SMBus(CHANNEL)

# ピンの入出力設定
writeByteData(ICADDR, REG_IODIR, 0x00)

while 1 :
	writeByteData(ICADDR, REG_OLAT, masks[ptr])
	time.sleep(1)
	ptr += 1
	if ptr == 3 :
		ptr = 0  



# GPA3, GPA5, GPA7 出力オン
writeByteData(ICADDR, REG_OLAT, 0xE8)
time.sleep(5)
# GPA3, GPA5, GPA7 出力オフ
writeByteData(ICADDR, REG_OLAT, 0x00)
'''

if __name__ == '__main__':
#
# ボードのテストを兼ねての、このモジュールを使うためのサンプルコード
#   ・A1,B1,C1,A2...A4,B4,C4 の順番にすべての LED を点灯する
#   ・A1,B1,C1,A2...A4,B4,C4 の順番にすべての LED を消灯する
#   ・メインの盤の上に駒を置いたらその場所の赤 LED が点灯
#   ・駒台の上に置いたら対応する緑 LED が点灯
#
	db = DobutuBoard()
	# 順にすべての LED を点灯する
	for y in range(4):
		for x in range(3):
			db.setLedState(x, y, db.RED, True)
			time.sleep(0.2)
			db.setLedState(x, y, db.GREEN, True)
			time.sleep(0.2)

	# 順にすべての LED を消灯する
	for y in range(4):
		for x in range(3):
			db.setLedState(x, y, db.RED, False)
			time.sleep(0.2)
			db.setLedState(x, y, db.GREEN, False)
			time.sleep(0.2)
			
	while True:
		time.sleep(0.2)
		db.getSwState()
		for y in range(4):
			for x in range(3):
				if db.mainBoardSwStatus[y][x]:
					db.setLedState(x, y, db.RED, True)
				else:
					db.setLedState(x, y, db.RED, False)
		
		for i in range(db.SUB_BOARD_SW_NUM):
			if db.subBoardPlayFirstSwStatus[i]:
				db.setLedState(i % 3, int(i / 3), db.GREEN, True)
			else:
				db.setLedState(i % 3, int(i / 3), db.GREEN, False)

			if db.subBoardDrawFirstSwStatus[i]:
				db.setLedState(i % 3, int(i / 3) + 2, db.GREEN, True)
			else:
				db.setLedState(i % 3, int(i / 3) + 2, db.GREEN, False)
